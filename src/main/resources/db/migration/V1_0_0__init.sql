CREATE TABLE IF NOT EXISTS thing (
	id SERIAL NOT NULL,
	name character(255) NOT NULL
);

INSERT INTO thing (name) VALUES ('Dave');
INSERT INTO thing (name) VALUES ('Rob');
INSERT INTO thing (name) VALUES ('Sue');
INSERT INTO thing (name) VALUES ('Ringo');
INSERT INTO thing (name) VALUES ('Mike');