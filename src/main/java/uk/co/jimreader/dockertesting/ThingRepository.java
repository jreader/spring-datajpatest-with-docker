package uk.co.jimreader.dockertesting;

import org.springframework.data.repository.CrudRepository;

public interface ThingRepository extends CrudRepository<Thing, Long> {

	Thing findByName(String name);
}
