package uk.co.jimreader.dockertesting;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class DemoController {

	private final ThingRepository thingRepository;
	
	@GetMapping("/hello")
	public Iterable<Thing> hello() {
		return thingRepository.findAll();
	}
}
